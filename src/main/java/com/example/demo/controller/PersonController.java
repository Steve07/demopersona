package com.example.demo.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.*;
import com.example.demo.service.*;

@CrossOrigin
@RestController
@RequestMapping("/persons")
public class PersonController {


	@Autowired
	PersonService ps;

	@GetMapping("/all")
	public ArrayList<PersonModel> getAll() {
		return ps.getAll();
	}

	@RequestMapping("{id}")
	public PersonModel getPerson(@PathVariable("id") int id) {
		return ps.getPerson(id);
	}
}
