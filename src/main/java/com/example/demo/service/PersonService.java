package com.example.demo.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.example.demo.model.*;

@Service
public class PersonService {
	ArrayList<PersonModel> persons = new ArrayList<PersonModel>();
	public PersonService() {
		PersonModel p = new PersonModel();
		p.setId(1);
		p.setEdad("21");
		p.setNombre("Steve");
		p.setApellido("Quintanilla");
		persons.add(p);

		p = new PersonModel();
		p.setId(2);
		p.setEdad("25");
		p.setNombre("kevin");
		p.setApellido("Miranda");
		persons.add(p);
	}
	public PersonModel getPerson(int id) {
		for(PersonModel person:persons) {
			if(person.getId()==id) return person;
		}
	    return null;
	}
	public ArrayList<PersonModel> getAll() {
		return persons;
	}
}
